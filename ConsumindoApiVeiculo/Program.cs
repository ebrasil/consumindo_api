﻿using ExemploConverteJasonClasse.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiVeiculo
{
    class Program
    {
        static void Main(string[] args)
        {
            //url base API
            string url = "http://senacao.tk/objetos/veiculo_array";

            //consumindo API
            Veiculo Veiculo = BuscarVeiculo(url);

            Console.WriteLine(string.Format("Marca: {0} - Modelo: {1} - Ano: {2} - Quilometragem: {3}", Veiculo.Marca, Veiculo.Modelo, Veiculo.Ano, Veiculo.Quilometragem));

            foreach (string opcional in Veiculo.Opcionais)
            {
                Console.WriteLine(opcional);
            }


            Console.ReadLine();
        }

        //função destinada a consumir a API que retornará os dados
        public static Veiculo BuscarVeiculo(string url)
        {
            //instanciando o WebClient para chamadas HTTP 
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Veiculo Veiculo = JsonConvert.DeserializeObject<Veiculo>(content);

            return Veiculo;

        }
    }
}
