﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExemploConverteJasonClasse.Models
{
    class Veiculo
    {
        public string Marca { get; set; }

        public string Modelo { get; set; }

        public int Ano { get; set; }

        public int Quilometragem { get; set; }

        public List<string> Opcionais { get; set; }

        public Vendedor Vendedor { get; set; }
        public IEnumerable<string> Softwares { get; internal set; }
    }

    class Vendedor
    {
        public string Nome { get; set; }

        public int Idade { get; set; }

        public string Celular { get; set; }

        public string Cidade { get; set; }
    }
}
