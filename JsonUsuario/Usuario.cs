﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JsonUsuario
{
    class Usuario
    {
        public string Nome { get; set; }

        public Endereco Endereco { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Cidade { get; set; }

        public List<Qualificacao> Qualificacoes { get; set; }

        public List<string> Conhecimentos { get; set; }
   
    }

    class Endereco
    {
        public string Rua { get; set; }

        public int Numero { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Uf { get; set; }
    }

    class Qualificacao
    {
        public string Nome { get; set; }

        public string Instituicao { get; set; }

        public int Ano { get; set; }
    }

}
