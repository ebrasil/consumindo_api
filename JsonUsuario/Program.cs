﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace JsonUsuario
{
    class Program
    {
        static void Main(string[] args)
        {
            //url base API
            string url = "http://senacao.tk/objetos/usuario";

            //consumindo API
            Usuario Usuario = BuscarUsuario(url);

            Console.WriteLine(string.Format("Nome: {0} - Endereco: {1} - Telefone: {2} - Email: {3} - Cidade: {4}", Usuario.Nome, Usuario.Endereco, Usuario.Telefone, Usuario.Email, Usuario.Cidade));

            foreach (string conhecimento in Usuario.Conhecimentos)
            {
                Console.WriteLine(conhecimento);
            }


            Console.ReadLine();
        }

        //função destinada a consumir a API que retornará os dados
        public static Usuario BuscarUsuario(string url)
        {
            //instanciando o WebClient para chamadas HTTP 
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Usuario Usuario = JsonConvert.DeserializeObject<Usuario>(content);

            return Usuario;

        }
    }
}
