﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiViaCep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o Cep para iniciar sua busca");
            string cep = Console.ReadLine();

            string url = "http://viacep.com.br/ws/" + cep + "/json/";
            Endereco Endereco = BuscarEnderecoPorCep(url);

            Console.WriteLine(Endereco.Cep);
            Console.WriteLine(Endereco.Logradouro);
            Console.WriteLine(Endereco.Complemento);
            Console.WriteLine(Endereco.Bairro);
            Console.WriteLine(Endereco.Localidade);
            Console.WriteLine(Endereco.Uf);
            Console.WriteLine(Endereco.Unidade);
            Console.WriteLine(Endereco.IBGE);
            Console.WriteLine(Endereco.Gia);

            Console.ReadLine();
        }

        public static Endereco BuscarEnderecoPorCep(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Endereco Endereco = JsonConvert.DeserializeObject<Endereco>(content);

            return Endereco;
        }
    }
}
