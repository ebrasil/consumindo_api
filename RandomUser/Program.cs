﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Net;

namespace RandomUser
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isValidNumUsers;
            string nat;

            Console.WriteLine("Digite o número de usuários desejado");
            isValidNumUsers = int.TryParse(Console.ReadLine(), out int numero);

            if (!isValidNumUsers)
                numero = 1;

            Console.WriteLine("Digite a nacionalidade: " + "(AU, BR, CA, CH, DE, DK, ES, FI, FR, " + "GB, IE, IR, NO, NL, TR, US):");

            nat = Console.ReadLine();

            ArrayList nat_list = new ArrayList {"AU", "BR", "CA", "CH", "DE", "DK", "ES", "FI", "FR", "GB", "IE", "IR", "NO", "NL", "TR", "US" };

            if (!nat_list.Contains(nat.ToUpper()))
                nat = "BR";

            Console.ReadLine();
        }

        public static RandomUser BuscarRandomUser(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            RandomUser RandomUser = JsonConvert.DeserializeObject<RandomUser>(content);

            return RandomUser;
        }
    }
}
