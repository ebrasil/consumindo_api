﻿using ExemploConverteJasonClasse.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ExemploConverteJasonClasse
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            //url base API
            string url = "http://senacao.tk/objetos/computador_array";

            //consumindo API
            Computador Computador = BuscarComputador(url);

            Console.WriteLine(string.Format("Marca: {0} - Modelo: {1} - Memoria {2} - SSD {3}", Computador.Marca, Computador.Modelo, Computador.Memoria, Computador.SSD));

            foreach(string software in Computador.Softwares)
            {
                Console.WriteLine(software);
            }

            

            Console.ReadLine();
        }
        

        //função destinada a consumir a API que retornará os dados
        public static Computador BuscarComputador(string url)
        {
            //instanciando o WebClient para chamadas HTTP 
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);            

            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);

            return Computador;

        }
    }
}
